/*
 *  Project.cpp
 *
 *  Created by Mateusz Urbanek on 15/11/2017.
 *
 *  © 2017 MATEUSZ URBANEK ALL RIGHTS RESERVED
 *
 *
 */


#define _CRT_SECURE_NO_WARNINGS 


#include <stdio.h>
#include <conio.h>
#include <Windows.h>
#include <stdlib.h>
#include <time.h>
#include <dos.h>


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


#define ANSI_COLOR_RED     "\x1b[31m"	//HP
#define ANSI_COLOR_BLUE    "\x1b[34m"	//Not Used
#define ANSI_COLOR_GREEN   "\x1b[32m"	//Not Used
#define ANSI_COLOR_YELLOW  "\x1b[33m"	//Stamina											//defining colors
#define ANSI_COLOR_MAGENTA "\x1b[35m"	//Not Used
#define ANSI_COLOR_CYAN    "\x1b[36m"	//Not Used
#define ANSI_COLOR_RESET   "\x1b[0m"	//Reset to default


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------

FILE *savefile;																				//	file with save, storing data like health points.

FILE *enemysave;

FILE *bossave;

FILE *npcsave;

int chapter = 0;																			//	Default begining of game - chapter number

unsigned char click;

int enemyrand;

int npcrand;


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


struct characters
{
	int gender;																					//	Stores info about Gender of character.
	char name[20];																				//	Stores info about Name of character.
	int hp;																						//	Stores info about Health Points of character.
	int stamina;																				//	Stores info about Stamina Points of character.
	int attack;																					//	Stores info aboit Attack Points of character.
	int karma;
};

struct characters character;


//----------------------------------------------------------------------------------------------------------------------------------------------------


struct enemies
{
	char type[20];																			//	Stores info about Type of enemy.
	int hp;																					//	Stores info about Health Points of enemy.
	int stamina;																			//	Stores info about Stamina Points of enemy.
	int attack;																				//	Stores info aboit Attack Points of enemy.
};

struct enemies enemy;


//----------------------------------------------------------------------------------------------------------------------------------------------------


struct bosses
{
	char name[20];
	int hp;
	int stamina;
	int attack;

	struct spells
	{
		int attack1;
		int attack2;
		int attack3;
	};

	struct spells attacks;
};

struct bosses boss;


//----------------------------------------------------------------------------------------------------------------------------------------------------

struct npcs
{
	char name[20];
	int hp;
};

struct npcs npc;


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


void checker();

void checkenemy();


//----------------------------------------------------------------------------------------------------------------------------------------------------


void CLS();	//windows	

int gotch();

void fullscreen();

void quiter();

void died();

void sleep(long milli);	

void credits();

int self(void);				//Works but waits for parallel settings


//----------------------------------------------------------------------------------------------------------------------------------------------------


void loadsave();

void loadenemy();							//	Loaders

void loadboss();

//----------------------------------------------------------------------------------------------------------------------------------------------------

void pchapter();

void dialogue();

//----------------------------------------------------------------------------------------------------------------------------------------------------

void saver();

void statistics();

void enemystatistics();

void bosstatistics();

void enemystats(int chaptermodifier);

void choicenpc();						// Asking if you want to fight or run

void choiceenemy();						// Asking if you want to kill or speak

void bossfight();

void fight();

void gameplay();

void hpcheck();


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


int main()
{
	fullscreen();																//	fullscreen for console

	savefile = fopen("save.dat", "a+");										

	self();


//----------------------------------------------------------------------------------------------------------------------------------------------------


back:
	system("cls");	//windows	
	printf("\n\t   _____                     _       \n");
	printf("\t  / ____|                   (_)      \n");
	printf("\t | |     ___  ___ ___  _ __  _  __ _ \n");							//	Looks bad here, but outputs good.
	printf("\t | |    / _ \\/ __/ _ \\| '_ \\| |/ _` |\n");
	printf("\t | |___|  __/ (_| (_) | | | | | (_| |\n");
	printf("\t  \\_____\\___|\\___\\___/|_| |_|_|\\__,_|\n");
	printf("\n");
	printf("\n\t1. New Game\n\t2. Load Game \n\t3. Exit\n");
	
	// intest();

	fflush(stdin);
	click = gotch();
	if (click == '1')										
	{
	back1:
		
		CLS();	//windows	
		printf("\n\tCreate new character?\n\t\t1. Yes\n\t\t2. No \n");
		fflush(stdin);
		click = gotch();
		CLS();	//windows	

		if (click == 'y' || click == 'Y' || click == '1')										//YES
		{
			CLS();	//windows	
			printf("\n\tYES\n");
			sleep(1000);
			CLS();	//windows	

			fclose(savefile);
			savefile = fopen("save.dat", "w+");

			fprintf(savefile, "%d\n", chapter);

			printf("\n\tSelect gender:\n\t\t1. Male\n\t\t2. Female\n");

		back2:

			fflush(stdin);
			click = gotch();

			if (click == '1')
			{
				character.gender = 0;
				fprintf(savefile, "%d\n", character.gender);
				system("cls");	//windows	
				printf("\n\tChoosen Male\n");
				sleep(1000);
				CLS();	//windows	
			}

			else if (click == '2')
			{
				character.gender = 1;
				fprintf(savefile, "%d\n", character.gender);
				CLS();	//windows	
				printf("\n\tChoosen Female\n");
				sleep(1000);
				CLS();	//windows	
			}

			else if (click == 27)
			{
				fclose(savefile);
				savefile = fopen("save.dat", "w+");
				goto end;
			}

			else goto back2;													//	Wrong expression, just go back and try again.

		back3:

			system("cls");	//windows	
			printf("\n\tEnter name of your Character:\n");
			printf(ANSI_COLOR_RED);
			printf("\t\t(Max 20 characters, anything else will be ignored)\n");
			printf(ANSI_COLOR_RESET);
			printf("\n\t\t");
			scanf("%s", &character.name);

		back4:

			CLS();	//windows	
			printf("\n\t%s\n", character.name);
			printf("\n\tAre you sure?\n\t\t1 - Yes\n\t\t2 - Enter name again");
			fflush(stdin);
			click = gotch();
			if (click == 49)
			{
				system("cls");	//windows	
				printf(ANSI_COLOR_GREEN);
				printf("\n\tYour name is %s\n", character.name);
				printf(ANSI_COLOR_RESET);
				fprintf(savefile, "%s\n", character.name);

				character.hp = 100;
				character.stamina = 50;
				character.attack = 10;
				character.karma = 0;

				fprintf(savefile, "%d\n", character.hp);
				fprintf(savefile, "%d\n", character.stamina);
				fprintf(savefile, "%d\n", character.attack);

				sleep(2000);
				CLS();	//windows	
				fclose(savefile);

				statistics();

				system("pause");	//windows
			}

			else if (click == 50) goto back3;

			else if (click == 27)
			{
				fclose(savefile);
				savefile = fopen("save.dat", "w+");
				goto end;
			}

			else goto back4;

			goto back;
		}

	//----------------------------------------------------------------------------------------------------------------------------------------------------


		else if (click == 'n' || click == 'N' || click == '2')									//NO
		{
			system("cls");	//windows										
			printf("\n\tNO\n");
			goto back;
		}

		else if (click == 27) goto end;

		else goto back1;
	}

	else if (click == '2')
	{
		loadsave();
		if ( character.hp == 0 ) 
		{
			system("cls");	//windows	
			printf("\n\tFile could not be opened to retrieve your data from it.\n");
			fclose(savefile);
			sleep(2000);
			goto back;
		}
		else
		{
			gameplay();
			checker();
		}
	}

	else if (click == 27 || click == '3') goto end;

	else goto back;

end:
	system("cls");	//windows	

end2:
	printf("\n");
	system("pause");	//windows

    return 0;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


int gotch()
{
	unsigned char miniclick;

	miniclick = getch();

	while (kbhit())														//	Prevents inputing more than 2 actions in a row,
	{																	//	Prevents inputing "1,1,1,1,1,...,1" into the buffer (?)
		fflush(stdin);
		miniclick = getch();
	}

	return miniclick;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void CLS()	//windows	
{
	system("cls");																//	Clear screen. With little addition
	printf("Press ESC to save and exit\n");
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void fullscreen()
{
	keybd_event(VK_MENU, 0x38, 0, 0);
	keybd_event(VK_RETURN, 0x1c, 0, 0);											//	Starts the app on fullscreen
	keybd_event(VK_RETURN, 0x1c, KEYEVENTF_KEYUP, 0);
	keybd_event(VK_MENU, 0x38, KEYEVENTF_KEYUP, 0);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void quiter()
{
	system("cls");	//windows	
	system("pause");	//windows
	exit(1);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void died()
{
	savefile = fopen("save.dat", "w");
	fclose(savefile);
	credits();
	quiter();
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void sleep(long milli)
{
	clock_t end, current = clock();
	for (end = current + milli; current < end; current = clock());
}					

//----------------------------------------------------------------------------------------------------------------------------------------------------

void credits()
{
	CLS();

	wchar_t copy = 184;
	printf("\n\n\t%c 2017 MATEUSZ URBANEK ALL RIGHTS RESERVED", copy);
	sleep(2000);
	CLS();

	printf("\n\n\tCreated by Mateusz Urbanek on 15/11/2017.");
	sleep(2000);
	CLS();
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


int self(void)
{
	int x = 0;
	const char *barRotate = "|\\-/";

	printf("\n\tLoading configuration files... ");

	for (int i = 0; i < 25; i++)
	{															
		if (x > 3) x = 0;
		printf("%c\b", barRotate[x++]);
		sleep(200);
	}

	return 0;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


void checker()
{
	system("cls");	//windows
	printf(ANSI_COLOR_GREEN);
	printf("\n\t\tSummary");
	printf(ANSI_COLOR_RESET);
	printf("\n\tChapter: %d\n", chapter);
	printf("\n\tName: %s\n", character.name);
	
	if (character.gender == 0)
	{
		printf("\n\tGender: Male\n");
	}

	else if (character.gender == 1)
	{
		printf("\n\tGender: Female\n");
	}

	printf("\n\tHealth Points: %d\n", character.hp);
	printf("\n\tStamina  Points: %d\n", character.stamina);
	printf("\n\tAttack Power: %d\n", character.attack);
	printf("\n\tKarma: %d\n", character.karma);

	printf("\n");

	system("pause");	//windows
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void checkenemy()
{
	system("cls");	//windows	
	printf("%s\n", enemy.type);
	printf("%d\n", enemy.hp);
	printf("%d\n", enemy.stamina);
	printf("%d\n", enemy.attack);

	printf("\n");

	system("pause");	//windows
}


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


void pchapter()
{
	printf("\n\t   _____ _                 _               \n");
	printf("\t  / ____| |               | |                \n");
	printf("\t | |    | |__   __ _ _ __ | |_ ___ _ __      \n");
	printf("\t | |    | '_ \\ / _` | '_ \\| __/ _ \\ '__|  \n");
	printf("\t | |____| | | | (_| | |_) | ||  __/ |        ");
	printf(ANSI_COLOR_GREEN);
	printf("\t===> %d <===\n", chapter);
	printf(ANSI_COLOR_RESET);
	printf("\t  \\_____|_| |_|\\__,_| .__/ \\__\\___|_|    \n");
	printf("\t                    | |                      \n");
	printf("\t                    |_|                      \n");
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void dialogue()
{
	system("cls");	//windows
	srand(time(NULL));
	
	int x = 0;
	const char *multidot = " . . .";

	printf("\n");

	int line1;

	line1 = rand() % 5;
	
	if (line1 == 0)
	{
		printf("\n\tHelo traveler!"); 
		sleep(1000);
	}

	else if (line1 == 1)
	{
		printf("\n\tWoah! Who are you?!"); 
		sleep(1000);
	}

	else if (line1 == 2)
	{
		printf("\n\tCome closer, I can\'t see well");
		for (int i = 0; i < 6; i++)
		{
			if (x > 3) x = 0;
			printf("%c", multidot[x++]);
			sleep(200);
		}
		sleep(1000);
	}

	else if (line1 == 3)
	{
		printf("\n\tSorry");
		
		if (character.gender == 0)
		{
			printf(" son");
		}

		else if (character.gender == 1)
		{
			printf(" daughter");
		}

		printf(", who are you?");
		sleep(1000);
	}

	else if (line1 == 4)
	{
		printf("\n\tCome here, hero, come to me.");
		sleep(1000);
	}

	int randomdial = rand() % 6;
	for (int i = 0; i <= randomdial; i++)
	{
		line1 = rand() % 20;
		if (line1 == 0)
		{

			printf("\n\tI don\'t like old cars. ");
			sleep(2000);													//	Quote from:
			printf("\n\tI mean they don\'t even interest me.");				//	J. D. Salinger
			sleep(2000);													//	'The Catcher in the Rye'
			printf("\n\tI\'d rather have a goddam horse. ");				//	Chapter 17.
			sleep(2000);
			printf("\n\tA horse is at least human, for God\'s sake.");
			sleep(2000);
		}

		else if (line1 == 1)
		{
			printf("\n\tMisery acquaints a man with strange bedfellows.");	// Quote from:
			sleep(2000);													// W. Shakespeare
																			// 'The Tempest'
																			// Trinculo, Act II, scene ii.
		}

		else if (line1 == 2)
		{
			printf("\n\tGive every man thy ear, but few thy voice.");		// Quote from:
			sleep(2000);													// W. Shakespeare
																			// 'Hamlet'
																			// Polonius, Act I, scene iii.
		}

		else if (line1 == 3)
		{
			printf("\n\tLord, what fools these mortals be!");				// Quote from:
			sleep(2000);													// W. Shakespeare
																			// 'A Midsummer Night's Dream'
																			// Puck, Act III, scene ii.
		}

		else if (line1 == 4)
		{
			printf("\n\tThe course of true love never did run smooth.");	// Quote from:
			sleep(2000);													// W. Shakespeare
																			// 'A Midsummer Night's Dream'
																			// Lysander, Act I, scene i.
		}

		else if (line1 == 5)
		{
			printf("\n\tCowards die many times before their deaths;");		// Quote from:
			sleep(2000);													// W. Shakespeare
			printf("\n\tThe valiant never taste of death but once.");		// 'Julius Caesar'
			sleep(2000);													// Caesar, Act II, scene ii.
																			
		}

		else if (line1 == 6)
		{
			printf("\n\tIs this a dagger which I see before me,");			// Quote from:
			sleep(2000);													// W. Shakespeare
			printf("\n\tThe handle toward my hand?");						// 'Macbeth'
			sleep(2000);													// Macbeth, Act II, scene i.
		}

		else if (line1 == 7)
		{
			printf("\n\tI am a man,");										// Quote from:
			sleep(2000);													// W. Shakespeare
			printf("\n\tMore sinn\'d against than sinning.");				// 'King Lear'
			sleep(2000);													// Lear, Act III, scene ii.
		}

		else if (line1 == 8)
		{
			printf("\n\tMonsters merge and welter through the water\'s mounting");	// Quote from:
			sleep(2000);															// A. Mickiewicz
			printf("\n\tDin. All hands, stand fast! A sailor sprints aloft,");		// 'Crimean Sonnets'
			sleep(2000);															// 'The Crossing'
			printf("\n\tHangs, swelling spider-like, among invisible nets");		
			sleep(2000);																
			printf("\n\tSurveys his slowly undulating snares, and waits.");			
			sleep(2000);																
		}

		else if (line1 == 9)
		{
			printf("\n\tLithuania, my country! You are as good health;");						// Quote from:
			sleep(2000);																		// A. Mickiewicz
			printf("\n\tHow much one should prize you, he only can tell, Who has lost you");	// 'Sir Thaddeus'
			for (int i = 0; i < 6; i++)															// Opening lines, translated by Marcel Weyland.
			{
				if (x > 3) x = 0;
				printf("%c", multidot[x++]);
				sleep(200);
			}
			sleep(2000);																			
		}				

		else if (line1 == 10)
		{
			printf("\n\tThe vulture doesn\'t eat your heart, but your brains.");	// Quote from:
			sleep(2000);															// J. Słowacki
																					// 'Agamemnon’s tomb'
																					// Translated by Mikos, Michal J.
		}

		else if (line1 == 11)
		{
			printf("\n\tAge does not make us childish, as they say.");		// Quote from:
			sleep(2000);													// J. W. Goethe
			printf("\n\tIt only finds us true children still.");			// 'Faust' Part I.
			sleep(1500);													// Prelude on the Stage
		}

		else if (line1 == 12)
		{
			printf("\n\tAnd here, poor fool! with all my lore");			// Quote from:
			sleep(2000);													// J. W. Goethe
			printf("\n\tI stand! no wiser than before.");					// 'Faust' Part I
			sleep(2000);													// Night, Faust in His Study.
		}

		else if (line1 == 13)
		{
			printf("\n\tDear friend, all theory is gray,");					// Quote from:
			sleep(2000);													// J. W. Goethe
			printf("\n\tAnd green the golden tree of life.");				// 'Faust' Part I
			sleep(2000);													// Mephistopheles and the Student.
		}

		else if (line1 == 14)
		{
			printf("\n\tI used to be an adventurer like you");				// Quote from:
			for (int i = 0; i < 6; i++)										// The Elder Scrolls V: Skyrim.
			{
				if (x > 3) x = 0;
				printf("%c", multidot[x++]);
				sleep(200);
			}
			sleep(500);	
			printf("\n\tBut then, I took an arrow in the knee");
			for (int i = 0; i < 6; i++)								
			{
				if (x > 3) x = 0;
				printf("%c", multidot[x++]);
				sleep(200);
			}
			sleep(2000);
		}

		else if (line1 == 15)
		{
			printf("\n\tHave you seen those awful zombies?");
			sleep(2000);
		}

		else if (line1 == 16)
		{
			printf("\n\tHave you seen those awful skeletals?");
			sleep(2000);
		}

		else if (line1 == 17)
		{
			printf("\n\tThe fatman killed my wife");
			for (int i = 0; i < 6; i++)
			{
				if (x > 3) x = 0;
				printf("%c", multidot[x++]);
				sleep(200);
			}
			sleep(2000);
		}

		else if (line1 == 18)
		{
			printf("\n\tWhat can we do with all those misfortunes");
			for (int i = 0; i < 6; i++)
			{
				if (x > 3) x = 0;
				printf("%c", multidot[x++]);
				sleep(200);
			}
			sleep(2000);
		}

		else if (line1 == 19)
		{
			printf("\n\tEverything I had was bourgeois as hell.");			//	Quote from:
			sleep(2000);													//	J. D. Salinger
																			//	'The Catcher in the Rye'
																			//	Chapter 17.
		}		
	}
	
	printf("\n\n");
	system("pause");
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void saver()
{
	fclose(savefile);
	savefile = fopen("save.dat", "w+");
	fprintf(savefile, "%d\n", chapter);
	fprintf(savefile, "%d\n", character.gender);
	fprintf(savefile, "%s\n", character.name);
	fprintf(savefile, "%d\n", character.hp);
	fprintf(savefile, "%d\n", character.stamina);
	fprintf(savefile, "%d\n", character.attack);
	fprintf(savefile, "%d\n", character.karma);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void statistics()
{
	char bar = 219;

	printf("\n\t%s\n", character.name);

	printf("\tHP: %d\t\t", character.hp);
	
	printf(ANSI_COLOR_RED);
	for (int i = 0; i <= character.hp; i++)
	{
		printf("%c", bar);
	}
	printf("\n");
	printf(ANSI_COLOR_RESET);

	printf("\tStamina: %d\t", character.stamina);

	printf(ANSI_COLOR_YELLOW);
	for (int i = 0; i <= character.stamina; i++)
	{
		printf("%c", bar);
	}
	printf(ANSI_COLOR_RESET);
	printf("\n\n");
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void enemystatistics()
{
	char bar = 219;

	printf("\n\n");

	printf("\n\t\t%s\n", enemy.type);

	printf("\t\tHP: %d\t\t", enemy.hp);

	printf(ANSI_COLOR_RED);
	for (int i = 0; i <= enemy.hp; i++)
	{
		printf("%c", bar);
	}
	printf("\n");
	printf(ANSI_COLOR_RESET);

	printf("\t\tStamina: %d\t", enemy.stamina);

	printf(ANSI_COLOR_YELLOW);
	for (int i = 0; i <= enemy.stamina; i++)
	{
		printf("%c", bar);
	}
	printf(ANSI_COLOR_RESET);
	printf("\n\n");
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void bosstatistics()
{
	char bar = 219;

	printf("\n\n");

	printf("\n\t\t%s\n", boss.name);

	printf("\t\tHP: %d\t\t", boss.hp);

	printf(ANSI_COLOR_RED);
	for (int i = 0; i <= boss.hp; i++)
	{
		printf("%c", bar);
		if (i == 99)
		{
			printf("\n\t\t\t\t");
		}
	}
	printf("\n");
	printf(ANSI_COLOR_RESET);

	printf("\t\tStamina: %d\t", boss.stamina);

	printf(ANSI_COLOR_YELLOW);
	for (int i = 0; i <= boss.stamina; i++)
	{
		printf("%c", bar);
		if (i == 99)
		{
			printf("\n\t\t\t\t");
		}
	}
	printf(ANSI_COLOR_RESET);
	printf("\n\n");
}


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


void loadsave()																	//	Reading data from save.
{
	//while ((click = getchar()) != '\n' && click != EOF);						//	Flushing the buffor(?)
	fseek(stdin, 0, SEEK_END);													//	Same but better
	fscanf(savefile, "%d", &chapter);
	fscanf(savefile, "%d", &character.gender);
	fscanf(savefile, "%s", &character.name);
	fscanf(savefile, "%d", &character.hp);
	fscanf(savefile, "%d", &character.stamina);
	fscanf(savefile, "%d", &character.attack);
	fscanf(savefile, "%d", &character.karma);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void loadenemy()
{
	
	if (enemyrand == 0)
	{
		enemysave = fopen("enemy0.dat", "r");
	}

	else if (enemyrand == 1)
	{
		enemysave = fopen("enemy1.dat", "r");
	}

	else if (enemyrand == 2)
	{
		enemysave = fopen("enemy2.dat", "r");
	}

	fseek(stdin, 0, SEEK_END);													//	Flushin the buffor
	fscanf(enemysave, "%s", &enemy.type);
	fscanf(enemysave, "%d", &enemy.hp);
	fscanf(enemysave, "%d", &enemy.stamina);
	fscanf(enemysave, "%d", &enemy.attack);

	fclose(enemysave);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void loadboss()
{
	bossave = fopen("boss.dat", "r");
	fseek(stdin, 0, SEEK_END);													//	Flushin the buffor
	fscanf(bossave, "%s", &boss.name);
	fscanf(bossave, "%d", &boss.hp);
	fscanf(bossave, "%d", &boss.stamina);
	fscanf(bossave, "%d", &boss.attack);

	fclose(bossave);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void loadnpc()
{
	if (npcrand == 0)
	{
		npcsave = fopen("npc0.dat", "r");
	}

	else if (npcrand == 1)
	{
		npcsave = fopen("npc1.dat", "r");
	}

	else if (npcrand == 2)
	{
		npcsave = fopen("npc2.dat", "r");
	}

	fseek(stdin, 0, SEEK_END);													//	Flushin the buffor
	fscanf(npcsave, "%s", &npc.name);
	fscanf(npcsave, "%d", &npc.hp);

	fclose(npcsave);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------


void enemystats(int chaptermodifier)
{
	enemy.hp = enemy.hp + chaptermodifier;
	enemy.stamina = enemy.stamina + chaptermodifier;

	if (chaptermodifier < 25)
	{
		enemy.attack = enemy.attack + chaptermodifier;
	}

	else
	{
		enemy.attack = enemy.attack + 25;
	}
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void fight()
{
	while (enemy.hp > 0)
	{
		CLS();	//windows	
		statistics();
		enemystatistics();

		if (character.stamina >= 15)
		{
			printf("\n\tChoose Action:");
			printf("\n\t\t1. Attack enemy.");
			printf("\n\t\t2. Heal yourself.");
			printf("\n\t\t3. Prepare defense.");
			printf("\n\t\t4. Rest.");
			printf("\n");

		backfight1:
			fflush(stdin);
			click = gotch();

			if (click == '1')
			{
				enemy.hp = enemy.hp - character.attack;
				character.stamina = character.stamina - 5;

				printf("\n\tAttacking.");
				sleep(1000);
				printf("\n\tEnemy HP: -%d", character.attack);
				sleep(1000);
				printf("\n\tYour Stamina: -5");

				sleep(1500);

				srand(time(NULL));
				int randomaction = rand() % 10;

				if (randomaction == 5 || enemy.stamina <= 0)
				{
					enemy.stamina = enemy.stamina + randomaction;
					printf("\n\tEnemy is resting.");
					sleep(1000);
					printf("\n\tEnemy Stamina: +%d", randomaction);
					sleep(2000);
				}

				else
				{
					character.hp = character.hp - enemy.attack;
					enemy.stamina = enemy.stamina - 5;
					printf("\n\tYour HP: -%d", enemy.attack);
					sleep(1000);
					printf("\n\tEnemy Stamina: -5");
					sleep(2000);
				}

				hpcheck();
			}

			else if (click == '2')
			{
				if (character.hp <= 150)
				{
					character.hp = character.hp + character.attack;

					printf("\n\tHealing.");
					sleep(1000);
					printf("\n\tYour HP: +%d", character.attack);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(1000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou got maximal Health.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(1000);
					}

					hpcheck();
				}
			}

			else if (click == '3')
			{
				character.stamina = character.stamina - 15;

				printf("\n\tPreparing Defense.");
				sleep(1000);
				printf("\n\tYour Stamina: -15");

				sleep(1500);

				srand(time(NULL));
				int randomaction = rand() % 10;

				enemy.stamina = enemy.stamina + randomaction;
				printf("\n\tEnemy is resting.");
				sleep(1000);
				printf("\n\tEnemy Stamina: +%d", randomaction);
				sleep(2000);

				hpcheck();
			}

			else if (click == '4')
			{
				if (character.stamina <= 150)
				{
					srand(time(NULL));
					int regen = rand() % 10;

					character.stamina = character.stamina + regen;

					printf("\n\tResting.");
					sleep(1000);
					printf("\n\tYour Stamina: +%d", regen);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack + 2;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack + 2);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou are well rested.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack + 2;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack + 2);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == 27)
			{
				saver();
				quiter();
			}

			else goto backfight1;
		}

		else if (character.stamina >= 5)
		{
			printf("\n\tChoose Action:");
			printf("\n\t\t1. Attack enemy.");
			printf("\n\t\t2. Heal yourself.");
			printf("\n\t\t3. Prepare defense.\t(unaviable - not enough stamina)");
			printf("\n\t\t4. Rest.");
			printf("\n");

		backfight2:
			fflush(stdin);
			click = gotch();

			if (click == '1')
			{
				enemy.hp = enemy.hp - character.attack;
				character.stamina = character.stamina - 5;

				printf("\n\tAttacking.");
				sleep(1000);
				printf("\n\tEnemy HP: -%d", character.attack);
				sleep(1000);
				printf("\n\tYour Stamina: -5");

				sleep(1500);

				srand(time(NULL));
				int randomaction = rand() % 10;

				if (randomaction == 5 || enemy.stamina <= 0)
				{
					enemy.stamina = enemy.stamina + randomaction;
					printf("\n\tEnemy is resting.");
					sleep(1000);
					printf("\n\tEnemy Stamina: +%d", randomaction);
					sleep(2000);
				}

				else
				{
					character.hp = character.hp - enemy.attack;
					enemy.stamina = enemy.stamina - 5;
					printf("\n\tYour HP: -%d", enemy.attack);
					sleep(1000);
					printf("\n\tEnemy Stamina: -5");
					sleep(2000);
				}

				hpcheck();
			}

			else if (click == '2')
			{
				if (character.hp <= 150)
				{
					character.hp = character.hp + character.attack;

					printf("\n\tHealing.");
					sleep(1000);
					printf("\n\tYour HP: +%d", character.attack);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(1000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou got maximal Health.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(1000);
					}

					hpcheck();
				}
			}

			else if (click == '4')
			{
				if (character.stamina <= 150)
				{
					srand(time(NULL));
					int regen = rand() % 10;

					character.stamina = character.stamina + regen;

					printf("\n\tResting.");
					sleep(1000);
					printf("\n\tYour Stamina: +%d", regen);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack + 2;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack + 2);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou are well rested.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack + 2;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack + 2);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == 27)
			{
				saver();
				quiter();
			}

			else goto backfight2;
		}

		else
		{
			printf("\n\tChoose Action:");
			printf("\n\t\t1. Attack enemy.\t(unaviable - not enough stamina)");
			printf("\n\t\t2. Heal yourself.");
			printf("\n\t\t3. Prepare defense.\t(unaviable - not enough stamina)");
			printf("\n\t\t4. Rest.");
			printf("\n");

		backfight3:
			fflush(stdin);
			click = gotch();

			if (click == '2')
			{
				if (character.hp <= 150)
				{
					character.hp = character.hp + character.attack;

					printf("\n\tHealing.");
					sleep(1000);
					printf("\n\tYour HP: +%d", character.attack);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(1000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou got maximal Health.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(1000);
					}

					hpcheck();
				}
			}

			else if (click == '4')
			{
				if (character.stamina <= 150)
				{
					srand(time(NULL));
					int regen = rand() % 10;

					character.stamina = character.stamina + regen;

					printf("\n\tResting.");
					sleep(1000);
					printf("\n\tYour Stamina: +%d", regen);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack + 2;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack + 2);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou are well rested.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || enemy.stamina <= 0)
					{
						enemy.stamina = enemy.stamina + randomaction;
						printf("\n\tEnemy is resting.");
						sleep(1000);
						printf("\n\tEnemy Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - enemy.attack + 2;
						enemy.stamina = enemy.stamina - 5;
						printf("\n\tYour HP: -%d", enemy.attack + 2);
						sleep(1000);
						printf("\n\tEnemy Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == 27)
			{
				saver();
				quiter();
			}

			else goto backfight3;
		}
	} 
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void fightboss()
{
	while (boss.hp > 0)
	{
		CLS();	//windows	
		statistics();
		bosstatistics();

		if (character.stamina >= 15)
		{
			printf("\n\tChoose Action:");
			printf("\n\t\t1. Attack boss.");
			printf("\n\t\t2. Heal yourself.");
			printf("\n\t\t3. Prepare defense.");
			printf("\n\t\t4. Rest.");
			printf("\n");

		backfight1:
			fflush(stdin);
			click = gotch();

			if (click == '1')
			{
				boss.hp = boss.hp - character.attack;
				character.stamina = character.stamina - 5;

				printf("\n\tAttacking.");
				sleep(1000);
				printf("\n\tBoss HP: -%d", character.attack);
				sleep(1000);
				printf("\n\tYour Stamina: -5");

				sleep(1500);

				srand(time(NULL));
				int randomaction = rand() % 10;

				if (randomaction == 5 || boss.stamina <= 0)
				{
					boss.stamina = boss.stamina + randomaction;
					printf("\n\tBoss is resting.");
					sleep(1000);
					printf("\n\tBoss Stamina: +%d", randomaction);
					sleep(2000);
				}

				else
				{
					character.hp = character.hp - boss.attack;
					boss.stamina = boss.stamina - 5;
					printf("\n\tYour HP: -%d", boss.attack);
					sleep(1000);
					printf("\n\tBoss Stamina: -5");
					sleep(2000);
				}

				hpcheck();
			}

			else if (click == '2')
			{
				if (character.hp <= 150)
				{
					character.hp = character.hp + character.attack;

					printf("\n\tHealing.");
					sleep(1000);
					printf("\n\tYour HP: +%d", character.attack);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou got maximal Health.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == '3')
			{
				character.stamina = character.stamina - 15;

				printf("\n\tPreparing Defense.");
				sleep(1000);
				printf("\n\tYour Stamina: -15");

				sleep(1500);

				srand(time(NULL));
				int randomaction = rand() % 10;

				boss.stamina = boss.stamina + randomaction;
				printf("\n\tBoss is resting.");
				sleep(1000);
				printf("\n\tBoss Stamina: +%d", randomaction);
				sleep(2000);

				hpcheck();
			}

			else if (click == '4')
			{
				if (character.stamina <= 150)
				{
					srand(time(NULL));
					int regen = rand() % 10;

					character.stamina = character.stamina + regen;

					printf("\n\tResting.");
					sleep(1000);
					printf("\n\tYour Stamina: +%d", regen);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou are well rested.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == 27)
			{
				saver();
				quiter();
			}

			else goto backfight1;
		}

		else if (character.stamina >= 5)
		{
			printf("\n\tChoose Action:");
			printf("\n\t\t1. Attack boss.");
			printf("\n\t\t2. Heal yourself.");
			printf("\n\t\t3. Prepare defense.\t(unaviable - not enough stamina)");
			printf("\n\t\t4. Rest.");
			printf("\n");

		backfight2:
			fflush(stdin);
			click = gotch();

			if (click == '1')
			{
				boss.hp = boss.hp - character.attack;
				character.stamina = character.stamina - 5;

				printf("\n\tAttacking.");
				sleep(1000);
				printf("\n\tBoss HP: -%d", character.attack);
				sleep(1000);
				printf("\n\tYour Stamina: -5");

				sleep(1500);

				srand(time(NULL));
				int randomaction = rand() % 10;

				if (randomaction == 5 || boss.stamina <= 0)
				{
					boss.stamina = boss.stamina + randomaction;
					printf("\n\tBoss is resting.");
					sleep(1000);
					printf("\n\tBoss Stamina: +%d", randomaction);
					sleep(2000);
				}

				else
				{
					character.hp = character.hp - boss.attack;
					boss.stamina = boss.stamina - 5;
					printf("\n\tYour HP: -%d", boss.attack);
					sleep(1000);
					printf("\n\tBoss Stamina: -5");
					sleep(2000);
				}

				hpcheck();
			}

			else if (click == '2')
			{
				if (character.hp <= 150)
				{
					character.hp = character.hp + character.attack;

					printf("\n\tHealing.");
					sleep(1000);
					printf("\n\tYour HP: +%d", character.attack);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou got maximal Health.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == '4')
			{
				if (character.stamina <= 150)
				{
					srand(time(NULL));
					int regen = rand() % 10;

					character.stamina = character.stamina + regen;

					printf("\n\tResting.");
					sleep(1000);
					printf("\n\tYour Stamina: +%d", regen);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou are well rested.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == 27)
			{
				saver();
				quiter();
			}

			else goto backfight2;
		}

		else
		{
			printf("\n\tChoose Action:");
			printf("\n\t\t1. Attack boss.\t(unaviable - not enough stamina)");
			printf("\n\t\t2. Heal yourself.");
			printf("\n\t\t3. Prepare defense.\t(unaviable - not enough stamina)");
			printf("\n\t\t4. Rest.");
			printf("\n");

		backfight3:
			fflush(stdin);
			click = gotch();

			if (click == '2')
			{
				if (character.hp <= 150)
				{
					character.hp = character.hp + character.attack;

					printf("\n\tHealing.");
					sleep(1000);
					printf("\n\tYour HP: +%d", character.attack);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou got maximal Health.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == '4')
			{
				if (character.stamina <= 150)
				{
					srand(time(NULL));
					int regen = rand() % 10;

					character.stamina = character.stamina + regen;

					printf("\n\tResting.");
					sleep(1000);
					printf("\n\tYour Stamina: +%d", regen);

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}

				else
				{
					printf("\n\tYou are well rested.");

					sleep(1500);

					srand(time(NULL));
					int randomaction = rand() % 10;

					if (randomaction == 5 || boss.stamina <= 0)
					{
						boss.stamina = boss.stamina + randomaction;
						printf("\n\tBoss is resting.");
						sleep(1000);
						printf("\n\tBoss Stamina: +%d", randomaction);
						sleep(2000);
					}

					else
					{
						character.hp = character.hp - boss.attack;
						boss.stamina = boss.stamina - 5;
						printf("\n\tYour HP: -%d", boss.attack);
						sleep(1000);
						printf("\n\tBoss Stamina: -5");
						sleep(2000);
					}

					hpcheck();
				}
			}

			else if (click == 27)
			{
				saver();
				quiter();
			}

			else goto backfight3;
		}
	}
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void choiceenemy()
{
	CLS();	//windows	
	statistics();

	printf("\n\tYou met %s, what are you doing?", enemy.type);
	printf("\n\t\t1. Fight him.");
	printf("\n\t\t2. Run away from him.");

backenemy:
	fflush(stdin);
	click = gotch();
	
	if (click == '1')
	{
		printf("\n");

		fight();
		if (enemy.hp == 0)
		{
			character.karma = character.karma + 1;
			character.hp = character.hp + 5;
			character.stamina = character.stamina + 5;
			character.attack = character.attack + 2;
		}
	}

	else if (click == '2')
	{
		printf("\n");
 
		character.karma = character.karma - 1;
		character.stamina = character.stamina - 5;
	}

	else if (click == 27)
	{
		saver();
		quiter();
	}

	else goto backenemy;

	sleep(1000);
	CLS();	//windows	
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void choicenpc()
{
	CLS();	//windows	
	statistics();

	printf("\n\tYou met %s, what you are you doing?", npc.name);
	printf("\n\t\t1. Speak to him.");
	printf("\n\t\t2. Assasinate him.");
	printf("\n");

backnpc:
	fflush(stdin);
	click = gotch();

	if (click == '1')
	{
		dialogue();
		character.karma = character.karma + 1;
		character.hp = character.hp + 5;
		character.stamina = character.stamina + 5;
		character.attack = character.attack + 3;
	}

	else if (click == '2')
	{
		npc.hp = 0;
		character.stamina = character.stamina - 5;
		character.karma = character.karma - 2;
		
		srand(time(NULL));
		int temprand = rand() % 3;
		
		if (temprand == 0) printf("\n\t%s:\tYou will burn in hell!", npc.name);
		else if (temprand == 1) printf("\n\t%s:\tKill yourself, you bad man!", npc.name);
		else if (temprand == 2) printf("\n\t%s:\tMy friends will kill you!", npc.name);
	}

	else if (click == 27)
	{
		saver();
		quiter();
	}

	else goto backnpc;

	sleep(2000);
	CLS();	//windows	
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void bossfight()
{
	CLS();	//windows	
	statistics();
	bosstatistics();
	loadboss();
	fightboss();
}


//----------------------------------------------------------------------------------------------------------------------------------------------------


void gameplay()
{
	CLS();	//windows	
	statistics();

	sleep(2000);
	for (chapter; chapter <= 100; chapter++)
	{
		system("cls");	//windows	
		pchapter();
		sleep(2000);
		system("cls");	//windows	

		srand(time(NULL));
		int eventor = rand() % 100;

		if (eventor >= 0 && eventor <= 59 && chapter != 100)
		{
			srand(time(NULL));
			enemyrand = rand() % 3;
			loadenemy();
			enemystats(chapter);
			choiceenemy();
		}

		else if (eventor == 60 || chapter == 100)
		{
			loadboss();
			fightboss();
		}

		else
		{
			srand(time(NULL));
			npcrand = rand() % 3;
			loadnpc();
			choicenpc();
		}
	}
}



//----------------------------------------------------------------------------------------------------------------------------------------------------


void hpcheck()
{
	if (character.hp <= 0)
	{
		printf("\a");
		printf(ANSI_COLOR_RED);
		printf("\n\n\t\t\tYou DIED\a");
		printf(ANSI_COLOR_RESET);

		sleep(2000);

		checker();

		died();

		quiter();
	}
}